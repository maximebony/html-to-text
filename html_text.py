import re
import codecs

# 4 groups to find either <p>, <a>, <h1/2/3/4/5/6> or <span>
reg = re.compile(r"<p\s?>(?P<txtp>.+)</p\s?>|<a href=(.+)\s?>(?P<txta>.+)</a\s?>|<h\d\s?>(?P<txth>.+)</h\d\s?>|<span>(?P<txts>.+)</span>")

# open and read html file
f = codecs.open(r"C:\Users\maxim\Desktop\test2.html", 'r', 'utf-8')
content= f.read()

# match regex with text and deleting remaining tags
dict_txt = dict()

dict_txt['txtp'] = [re.sub('<[^>]*>', '', match.group("txtp")) for match in reg.finditer(content) if match.group("txtp") is not None]
dict_txt['txta'] = [re.sub('<[^>]*>', '', match.group("txta")) for match in reg.finditer(content) if match.group("txta") is not None]
dict_txt['txth'] = [re.sub('<[^>]*>', '', match.group("txth")) for match in reg.finditer(content) if match.group("txth") is not None]
dict_txt['txts'] = [re.sub('<[^>]*>', '', match.group("txts")) for match in reg.finditer(content) if match.group("txts") is not None]

for res in dict_txt.values():
    print("\n", res)
